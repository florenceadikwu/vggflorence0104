function displayList () {
    let getFromLocal = JSON.parse(localStorage.getItem("allEntries"));
    
    if(getFromLocal == null) existingEntries = [];
    else{
    let testing = getFromLocal.reduce(function (acc, curr) { 
       return acc.concat(curr)
      }, [] )
      console.log(testing)
    
    let jobListContainer = document.getElementById("client");
   
    testing.map((result, index) => {
       const { "job-title": jobTitle, description, "create-date": createDate, "close-date": closeDate, roles, salary, city, requirements, apply } = result;
       console.log(result);
       
 
       var item = document.createElement("div");
 
       item.innerHTML =
        `<div class="acc-kontainer">          
            <div>
            <input type="radio" name="acc" id="acc${index}" checked/>
            <label for="acc${index}">NEW OPENING! <i class="fas fa-arrow-right"></i>  ${jobTitle}</label>
            <div class="acc-body">
                DESCRIPTION: ${description}
            </div>
            <div class="acc-body">
                START DATE: ${createDate}
            </div>
            <div class="acc-body">
                CLOSE DATE: ${closeDate}
            </div>
            <div class="acc-body">
                ROLE DESCRIPTION: ${roles}
            </div>
            <div class="acc-body">
                SALARY: ${salary}
            </div>
            
            <div class="acc-body">
                CITY: ${city}
            </div>
            <div class="acc-body">
                REQUIREMENTS: ${requirements}
            </div>
            </div>

        </div>
    ` 
 
       jobListContainer.appendChild(item);
 
   });
 
}
           
       }
 
       displayList()